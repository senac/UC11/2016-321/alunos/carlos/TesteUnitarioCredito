package br.com.senac.calculadoracaixaeconomica;


public class Cliente {
    
    private int codigo;
    private String nome;
    private double percentualCalculado;
    private SaldoMedio saldoMedio;

    public Cliente() {
    }

    public Cliente(int codigo, String nome, double percentualCalculado) {
        this.codigo = codigo;
        this.nome = nome;
        this.percentualCalculado = percentualCalculado;
        this.saldoMedio = saldoMedio;
    }

    public int getCodigo() {
        return codigo;
    }

    public void setCodigo(int codigo) {
        this.codigo = codigo;
    }

    public String getNome() {
        return nome;
    }

    public void setNome(String nome) {
        this.nome = nome;
    }

    public double getPercentualCalculado() {
        return percentualCalculado;
    }

    public void setPercentualCalculado(double percentualCalculado) {
        this.percentualCalculado = percentualCalculado;
    }

    public SaldoMedio getSaldoMedio() {
        return saldoMedio;
    }

    public void setSaldoMedio(SaldoMedio saldoMedio) {
        this.saldoMedio = saldoMedio;
    }
    
    

    
   
    
    
}
