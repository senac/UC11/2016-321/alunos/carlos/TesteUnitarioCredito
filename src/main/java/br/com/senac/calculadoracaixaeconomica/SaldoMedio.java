
package br.com.senac.calculadoracaixaeconomica;

public enum SaldoMedio {
    
    SALDO_0_500, 
    SALDO_501_1000,
    SALDO_1001_3000,
    SALDO_3001
    
}
