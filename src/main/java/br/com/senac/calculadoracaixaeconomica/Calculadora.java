package br.com.senac.calculadoracaixaeconomica;

public class Calculadora {

    public double SaldoEspecial(Cliente cliente, SaldoMedio saldoMedio) {

        switch (saldoMedio) {
            case SALDO_0_500:
                return cliente.getPercentualCalculado();
            case SALDO_501_1000:
                return cliente.getPercentualCalculado() * 0.30;
            case SALDO_1001_3000:
                return cliente.getPercentualCalculado() * 0.40;
            case SALDO_3001:
                return cliente.getPercentualCalculado()*0.50;
            default:
                break;
        }
        return 0;
    }
}
