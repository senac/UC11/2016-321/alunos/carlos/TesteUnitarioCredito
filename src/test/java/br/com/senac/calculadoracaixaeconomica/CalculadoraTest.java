
package br.com.senac.calculadoracaixaeconomica;

import junit.framework.Assert;
import org.junit.Test;


public class CalculadoraTest {
    
   @Test
   public void deveCalcularSaldoMedioDe0A500(){
      
       Calculadora calculadora = new Calculadora();
       Cliente cliente = new Cliente(1,"jose",0);
       
       double resultado = calculadora.SaldoEspecial(cliente, SaldoMedio.SALDO_0_500);
       Assert.assertEquals(0, resultado, 0.01);
       
      
       
   }
   
   @Test
   public void deveCalcularSaldoMedioDe501A1000(){
      
       Calculadora calculadora = new Calculadora();
       Cliente cliente = new Cliente(1,"jose",1000);
       
       double resultado = calculadora.SaldoEspecial(cliente, SaldoMedio.SALDO_501_1000);
       Assert.assertEquals(300, resultado, 0.01);
       
      
       
   }
   
   @Test
   public void deveCalcularSaldoMedioDe1001A3000(){
      
       Calculadora calculadora = new Calculadora();
       Cliente cliente = new Cliente(1,"jose",2000);
       
       double resultado = calculadora.SaldoEspecial(cliente, SaldoMedio.SALDO_1001_3000);
       Assert.assertEquals(800, resultado, 0.01);
       
      
       
   }
  @Test
   public void deveCalcularSaldoMedioDe3001(){
      
       Calculadora calculadora = new Calculadora();
       Cliente cliente = new Cliente(1,"jose",4000);
       
       double resultado = calculadora.SaldoEspecial(cliente, SaldoMedio.SALDO_3001);
       Assert.assertEquals(2000, resultado, 0.01);
       
      
       
   }
    
}
